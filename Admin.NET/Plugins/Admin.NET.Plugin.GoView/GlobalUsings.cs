﻿// 此源代码遵循位于源代码树根目录中的 LICENSE 文件的许可证。
//
// 必须在法律法规允许的范围内正确使用，严禁将其用于非法、欺诈、恶意或侵犯他人合法权益的目的。

global using Admin.NET.Core;
global using Admin.NET.Core.Service;
global using Admin.NET.Plugin.GoView.Const;
global using Admin.NET.Plugin.GoView.Entity;
global using Admin.NET.Plugin.GoView.Enum;
global using Admin.NET.Plugin.GoView.Service.Dto;
global using Furion;
global using Furion.DatabaseAccessor;
global using Furion.DataValidation;
global using Furion.DynamicApiController;
global using Furion.FriendlyException;
global using Furion.JsonSerialization;
global using Furion.UnifyResult;
global using Mapster;
global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Http;
global using Microsoft.AspNetCore.Mvc;
global using Microsoft.AspNetCore.Mvc.Filters;
global using Newtonsoft.Json;
global using SqlSugar;
global using System.Collections;
global using System.ComponentModel;
global using System.ComponentModel.DataAnnotations;
global using System.Data;
global using System.Linq.Dynamic.Core;