﻿// 此源代码遵循位于源代码树根目录中的 LICENSE 文件的许可证。
//
// 必须在法律法规允许的范围内正确使用，严禁将其用于非法、欺诈、恶意或侵犯他人合法权益的目的。

namespace Admin.NET.Application.Const;

/// <summary>
/// 业务应用相关常量
/// </summary>
public class ApplicationConst
{
    /// <summary>
    /// API分组名称
    /// </summary>
    public const string GroupName = "业务应用";
}